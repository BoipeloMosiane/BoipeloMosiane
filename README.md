- 👋 Hi, I’m @BoipeloMosiane
- 👀 I’m interested in Data Science
- 🌱 I’m currently learning Big Data Technologies, with hopes to pursue the Data Science PGDip
- 💞️ I’m looking to collaborate on Big data Projects
- 📫 How to reach me: Mosiane.boipelo@gmail.com

<!---
BoipeloMosiane/BoipeloMosiane is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
